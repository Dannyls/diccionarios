#___Autor:"Danny Lima"____
#___Email:danny.lima@unl.edu.ec

# Dada una lista de países y ciudades de cada país, luego dados
# los nombres de las ciudades. Para cada ciudad imprimir el país
# en el que se encuentra.
# Read a string:
s = int(input())
# Print a value:
# print(s)
ciudad_pais = {}
for i in range(s):
    pais, *ciudades = input().split()
    for ciudad in ciudades:
        ciudad_pais[ciudad] = pais

a = int(input())
for i in range(a):
    print(ciudad_pais[input()])
